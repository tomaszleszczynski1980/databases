-- Creating materialized view: all scenes that character appears in
drop materialized view if exists scenes_for_character;

create materialized view scenes_for_character as
select character.id                                                                    as id,
       character.name                                                                  as name,
       concat(scenes.series_number, scenes.scene_number, scenes.scene_number_appendix) as scene_full_number
from scenes
         join scenes_to_characters
              on scenes.series_number = scenes_to_characters.scene_series_number
                  and scenes.scene_number = scenes_to_characters.scene_number
                  and scenes.scene_number_appendix = scenes_to_characters.scene_number_appendix
         join character
              on character.id = scenes_to_characters.character_id;


-- Creating view: all scenes that actor plays in
drop view if exists scenes_for_actor;

create view scenes_for_actor as
select actor.id as id, actor.name, scenes_for_character.scene_full_number
from scenes_for_character
         right join actor
                    on actor.id = scenes_for_character.id
union
select actor.id, actor.name, scenes_for_character.scene_full_number
from scenes_for_character
         join character_to_actor
              on scenes_for_character.id = character_to_actor.character_id
         right join actor
                    on character_to_actor.actor_id = actor.id;


-- Creating view: all real locations actor plays in
drop view if exists actors_in_real_location;

create view actors_in_real_location as
select real_location.id   as location_id,
       real_location.name as location_name,
       real_location.address,
       actor_id           as actor_id,
       actor_character.name
from real_location
         join script_to_real_location on real_location.id = script_to_real_location.real_location
         join script_location on script_to_real_location.script_location = script_location.name
         join scenes on script_location.name = scenes.script_location
         join scenes_to_characters
              on scenes.series_number = scenes_to_characters.scene_series_number
                  and scenes.scene_number = scenes_to_characters.scene_number
                  and scenes.scene_number_appendix = scenes_to_characters.scene_number_appendix
         join character on scenes_to_characters.character_id = character.id
         join (select character.id as character_id, actor.id as actor_id, actor.name
               from character
                        join actor on character.id = actor.character_id
               union
               select character.id as character_id, actor.id as actor_id, actor.name
               from character
                        join character_to_actor on character.id = character_to_actor.character_id
                        join actor on character_to_actor.actor_id = actor.id) as actor_character
              on character.id = actor_character.actor_id;


-- Create view scenes that are shoot in given real location
drop view if exists scenes_in_real_location;

create view scenes_in_real_location as
select real_location.id,
       real_location.name,
       concat(series_number, scene_number, scene_number_appendix) as scene_full_number
from real_location
         join script_to_real_location on real_location.id = script_to_real_location.real_location
         join script_location on script_to_real_location.script_location = script_location.name
         join scenes on script_location.name = scenes.script_location;
