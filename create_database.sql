-- this script creates relations in Movie Scheduler App Database
-- author: Tomasz Leszczynski

drop type if exists roles;
drop type if exists character_status;
drop type if exists loc;

create type roles as enum ('Actor', 'Body Double', 'Stunt Double', 'Voice Double', 'Stand-in', 'Stunt', 'Extra');
create type character_status as enum ('Main', 'Secondary', 'Third Plan', 'Episode', 'Special');
create type loc as enum ('Interior', 'Exterior');

create table if not exists Contacts
(
    id serial not null primary key,
    name varchar(100) not null,
    description text,
    phone1 varchar(15),
    phone2 varchar(15),
    email1 varchar(254),
    email2 varchar(254),
    other1_type varchar(20),
    other1 varchar(254),
    other2_type varchar(20),
    other2 varchar(254)
);

create table if not exists Real_location
(
    id serial not null primary key,
    name varchar(100) not null,
    address varchar(200) not null,
    GPS_coordinates point,
    contact int not null,

    constraint contacts_fk
		foreign key (contact) references contacts (id)
			on update no action on delete restrict
);

create table if not exists Script_location
(
  name varchar(20) unique primary key,
  description text
);

-- Switch table
create table if not exists Script_to_real_location
(
    script_location varchar(20) unique references Script_location(name) on delete cascade on update cascade,
    real_location int unique references Real_location(id) on delete cascade on update cascade
);

create table if not exists Character
(
    id serial not null primary key,
    name varchar(50),
    status character_status
);

drop table if exists Actor;

create table if not exists Actor
(
    id serial not null primary key,
    character_id int unique,
    name varchar(50) not null,
    other_names varchar(50),
    role roles default 'Actor',
    agent varchar(100),
    contact int,

    constraint character_fk
        foreign key (character_id) references Character(id)
            on delete cascade on update cascade,

    constraint contacts_fk
            foreign key (contact) references contacts (id)
                on update no action on delete restrict
);

-- Switch table
create table if not exists Character_to_actor
(
    character_id int unique references Character(id) on delete cascade on update cascade,
    actor_id int unique references Actor(id) on delete cascade on update cascade
);

create table if not exists Year_season
(
    season_name varchar(10) not null unique primary key,
    weather_conditions text
);

create table if not exists Daytime
(
    daytime_name varchar(10) not null unique primary key
);

create table if not exists Scenes
(
    series_number int,
    scene_number int not null,
    scene_number_appendix varchar(5),
    int_ext varchar(10) default 'Interior',
    description text not null,
    daytime varchar(10) not null references daytime(daytime_name),
    year_season varchar(10) references year_season(season_name),
    script_location varchar(20) references script_location(name),
    pages float not null,
    timing float,

    primary key (series_number, scene_number, scene_number_appendix),
    unique (series_number, scene_number, scene_number_appendix)
);

-- Switch table
create table if not exists Scenes_to_characters
(
    character_id int references Character(id) on delete cascade on update cascade,
    scene_series_number int,
    scene_number int,
    scene_number_appendix varchar(5),

    unique (scene_series_number, scene_number, scene_number_appendix),
    constraint scene_full_number_fk foreign key (scene_series_number, scene_number, scene_number_appendix)
        references Scenes(series_number, scene_number, scene_number_appendix)
);
