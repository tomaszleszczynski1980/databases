-- This script creates logger that logs all modifications made on Actor table

-- Create enum type to represent operation
create type methods as enum('insert', 'update', 'delete');

-- Create table with logs
create table if not exists actor_logs
(
    date_time timestamp default now() primary key,
    method methods not null,
    actors_id int not null,
    actors_name varchar(50) not null,
    new_role roles
);

-- Create trigger function on insert
create or replace function logger_insert()
    returns trigger
    language plpgsql
as
$$
begin
    insert into actor_logs (method, actors_id, actors_name, new_role)
    values ('insert', new.id, new.name, new.role);
end;
$$;

-- Create trigger function on update
create or replace function logger_update()
    returns trigger
    language plpgsql
as
$$
begin
    insert into actor_logs (method, actors_id, actors_name, new_role)
    values ('update', old.id, new.name, new.role);
end;
$$;

-- Create trigger function on update
create or replace function logger_delete()
    returns trigger
    language plpgsql
as
$$
begin
    insert into actor_logs (method, actors_id, actors_name)
    values ('update', old.id, old.name);
end;
$$;

-- Create triggers - on insert, on update, on delete
create trigger log_insert_actor
    after insert
    on actor
    for each row
    execute procedure logger_insert();

create trigger log_update_actor
    after update
    on actor
    for each row
    execute procedure logger_update();

create trigger log_delete_actor
    before delete
    on actor
    for each row
    execute procedure logger_delete();

