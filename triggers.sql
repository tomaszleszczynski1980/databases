-- Trigger functions
create or replace function add_daytime()
    returns trigger
    language plpgsql
as
$$
begin
    if new.daytime not in (select * from daytime) then
        insert into daytime (daytime_name) values (new.daytime);
    end if;

    return new;
end;
$$;


create or replace function add_year_season()
    returns trigger
    language plpgsql
as
$$
begin
    if new.year_season not in (select season_name
                               from year_season) then
        insert into year_season (season_name)
        values (new.year_season);
    end if;

    return new;
end;
$$;


create or replace function set_scenes_timing()
    returns trigger
    language plpgsql
as
$$
begin
    new.timing = new.pages * 2;

    return new;
end;
$$;


create or replace function set_actor_agent()
    returns trigger
    language plpgsql
as
$$
begin
    if new.agent is null then
        new.agent = new.name;
    end if;

    return new;
end;
$$;


-- Triggers
create trigger add_daytime_trigger
    before insert or update
    on scenes
    for each row
    execute procedure add_daytime();


create trigger add_year_season_trigger
    before insert or update
    on scenes
    for each row
    execute procedure add_year_season();


create trigger scenes_timing_trigger
    before insert or update
    on scenes
    for each row
    execute procedure set_scenes_timing();


create trigger set_actor_agent_trigger
    before insert or update
    on actor
    for each row
    execute procedure set_actor_agent();
