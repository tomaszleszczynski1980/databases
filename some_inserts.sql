insert into daytime (daytime_name) values ('dusk');

insert into year_season (season_name) values ('autumn');

insert into script_location (name, description) VALUES ('Forest', 'This is deep forest');
insert into script_location (name, description) VALUES ('Kelly House', 'This is Kelly House');
insert into script_location (name, description) VALUES ('Woods', 'Some woods arround city');

insert into scenes (series_number, scene_number, scene_number_appendix, description, daytime, year_season, script_location, pages)
values (2, 12, 'E',
        'This is the test 2', 'night', 'summer', 'Forest', 2);

insert into character (name, status) VALUES ('John', 'Main');
insert into character (name, status) values ('Susna', 'Secondary');


