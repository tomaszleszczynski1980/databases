-- Indexes

-- Index on Scenes interior
create index if not exists int_ext_index
on Scenes (int_ext)
    where int_ext = 'Interior';

-- Index on Scenes exterior
create index if not exists int_ext_index
on Scenes (int_ext)
    where int_ext = 'Exterior';

--Index on Scenes daytime
create index if not exists daytime_index
on Scenes (daytime);

--Index on Scenes script location
create index if not exists script_location_index
on Scenes (script_location);

-- Index on Actor by agent
create index if not exists actors_agent_index
on Actor (agent)
    where agent is not null;
