-- this script creates relations in Movie Scheduler App Database
-- author: Tomasz Leszczynski

create table if not exists Character
(
    id serial not null primary key,
    name varchar(50),
    status int);



create table if not exists Scenes
(
    -- id serial unique not null,
    series_number int,
    scene_number int not null,
    scene_number_appendix varchar(5),
    int_ext varchar(10) default 'Interior',
    description text not null,
    daytime varchar(10) null ,
    year_season varchar(10),
    script_location varchar(20) ,
    pages float not null,
    primary key ( series_number ,
    scene_number ,
    scene_number_appendix),
    unique (series_number, scene_number, scene_number_appendix)
);

-- Switch table
-- does not work
create table if not exists Scenes_to_characters
(
    character_id int references Character(id) on delete cascade on update cascade,
    scene_series_number int ,
    scene_number int ,
    scene_number_appendix varchar(5) ,
    unique (scene_series_number, scene_number, scene_number_appendix),
    CONSTRAINT scene_full_number_fk foreign key (scene_series_number, scene_number, scene_number_appendix)
        references Scenes(series_number, scene_number, scene_number_appendix)
);
